import React from 'react'
import { ActivityIndicator } from 'react-native'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import ReduxThunk from 'redux-thunk'
import reducers from './src/reducers'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'
import BaseNavigatorContainer from "./src/navigation/BaseNavigator"
import { useFonts, Poppins_600SemiBold, Poppins_500Medium, Poppins_300Light, Poppins_700Bold } from '@expo-google-fonts/poppins'
import styled from 'styled-components/native'
import { BACKGROUND_COLOR } from './src/actions/types'

const persistConfig = {
  key: 'AuthLoading',
  storage: AsyncStorage,
}

const pReducer = persistReducer(persistConfig, reducers);

const store = compose(
  applyMiddleware(ReduxThunk)
)(createStore)(pReducer);

const persistor = persistStore(store);

export const LoadingContainer = styled.View`
  flex: 1 100%;
  background-color: ${BACKGROUND_COLOR};
  align-items: center;
  justify-content: center;
`

const App = () => {
  let [fontsLoaded] = useFonts({
    Poppins_600SemiBold,
    Poppins_500Medium,
    Poppins_300Light,
    Poppins_700Bold
  });
  if (!fontsLoaded) {
    return (
      <LoadingContainer>
        <ActivityIndicator size="large" />
      </LoadingContainer>
    )
  }
  return (
    <>
      <Provider store={store}>
        <BaseNavigatorContainer screenProps={store} />
      </Provider>
    </>
  );
};

export default App;
