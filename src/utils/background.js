export const getBackgroundGradient = (icon_code) => {
    switch (icon_code) {
        case '01d':
            return ['#5374E7', '#77B9F5']
        case '02d':
            return ['#5374E7', '#77B9F5']
        case '03d':
            return ['#464C64', '#99A9B9']
        case '04d':
            return ['#464C64', '#99A9B9']
        case '09d':
            return ['#011354', '#5B9FE3']
        case '10d':
            return ['#011354', '#5B9FE3']
        case '11d':
            return ['#011354', '#5B9FE3']
        case '13d':
            return ['#464C64', '#99A9B9']
        case '50d':
            return ['#464C64', '#99A9B9']
        // NIGHT
        case '01n':
            return ['#011354', '#2d3c75']
        case '02n':
            return ['#011354', '#2d3c75']
        case '03n':
            return ['#242733', '#5b646e']
        case '04n':
            return ['#242733', '#5b646e']
        case '09n':
            return ['#242b30', '#3f4b54']
        case '10n':
            return ['#242b30', '#3f4b54']
        case '11n':
            return ['#242b30', '#3f4b54']
        case '13n':
            return ['#242733', '#5b646e']
        case '50n':
            return ['#242733', '#5b646e']
        default:
            return ['#5374E7', '#77B9F5']
    }
}