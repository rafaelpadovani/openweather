import styled from 'styled-components/native'
import { BACKGROUND_COLOR, PRIMARY_COLOR } from '../../actions/types'

export const StyledSafeAreaView = styled.View`
  flex: 1 100%;
  background-color: ${BACKGROUND_COLOR};
  padding-top: ${(props) => props.insets?.top || 0}px;
  padding-bottom: 0px;
`
export const HeaderTitle = styled.Text`
  color: ${PRIMARY_COLOR};
  font-family: Poppins_600SemiBold;
  font-size: 28px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 42px;
  text-align: center;
  margin-top: 10px;
`
export const AddText = styled.Text`
  color: ${PRIMARY_COLOR};
  font-family: Poppins_600SemiBold;
  font-size: 20px;
  font-weight: 600;
  letter-spacing: 0;
  margin: 0px 10px;
  text-align: center;
`

export const ButtonContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 30px 0px 30px;
`

export const AddIcon = styled.Image`
  width: 26px;
  height: 26px;
`