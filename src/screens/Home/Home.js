import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { View, RefreshControl, FlatList, Alert } from 'react-native'
import { StyledSafeAreaView, HeaderTitle, AddText, ButtonContainer, AddIcon } from './styles'
import { FocusAwareStatusBar } from '../../components/UI'
import { BACKGROUND_COLOR, PRIMARY_COLOR } from '../../actions/types'
import { CityCard } from '../../components/Card'
import { multipleCities } from '../../actions/WeatherActions'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { LoadingUI } from '../../components/LoadingUI'

const Home = ({ multipleCities, cities, navigation }) => {
    const insets = useSafeAreaInsets();
    const [citiesIDList, setCitiesIDList] = useState(['2643743', '3165524', '3169070'])
    const [loading, setLoading] = useState(false)
    const [loaded, setLoaded] = useState(false)
    const errorHandler = () => {
        setLoading(false)
        Alert.alert("Si è verificato un errore, riprova più tardi")
    }
    useEffect(() => {
        fetchData()
    }, [])
    const fetchData = (refresh) => {
        if (refresh) {
            setLoading(true)
        }
        multipleCities(citiesIDList)
            .then(() => {
                setLoading(false)
                setLoaded(true)
            })
            .catch(() => errorHandler())
    }
    if (!cities || !loaded) {
        return (
            <LoadingUI />
        )
    }
    return (
        <StyledSafeAreaView insets={insets}>
            <FocusAwareStatusBar backgroundColor={BACKGROUND_COLOR} barStyle='dark-content' />
            <FlatList
                refreshControl={
                    <RefreshControl
                        tintColor={PRIMARY_COLOR}
                        refreshing={loading}
                        onRefresh={() => fetchData(true)}
                    />
                }
                showsVerticalScrollIndicator={false}
                ListHeaderComponent={
                    <View>
                        <HeaderTitle>{'Good Morning!\nMario'}</HeaderTitle>
                        <ButtonContainer>
                            <AddIcon source={require('../../assets/icons/Plus.png')} />
                            <AddText>Aggiungi citt&agrave;</AddText>
                        </ButtonContainer>
                    </View>
                }
                ListFooterComponent={<View style={{ height: 100 }} />}
                keyExtractor={(item) => item?.id.toString()}
                data={cities}
                renderItem={({ item }) => (
                    <CityCard city={item} onPress={() => navigation.navigate('CityDetail', { city: item })} />
                )}
            />
        </StyledSafeAreaView>
    )
}

const mapStateToProps = state => ({
    cities: state.menu.cities
});

export default connect(mapStateToProps, { multipleCities })(Home);