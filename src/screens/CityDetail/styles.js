import { SafeAreaView } from 'react-native'
import styled from 'styled-components/native'
import { BACKGROUND_COLOR, PRIMARY_COLOR } from '../../actions/types'

export const StyledSafeAreaView = styled.SafeAreaView`
  flex: 1 100%;
  background-color: ${BACKGROUND_COLOR}
`

export const CurrentDate = styled.Text`
  color: #FFFFFF;
  font-family: Poppins_500Medium;
  font-size: 20px;
  font-weight: 500;
  letter-spacing: 0;
  text-align: center;
`

export const WeatherCondition = styled.Text`
  color: #FFFFFF;
  font-family: Poppins_300Light;
  font-size: 20px;
  font-weight: 300;
  letter-spacing: 0;
  line-height: 30px;
  margin: 20px 0px 10px;
  text-align: center;
`

export const Temperature = styled.Text`
  color: #FFFFFF;
  font-family: Poppins_700Bold;
  font-size: 90px;
  font-weight: bold;
  letter-spacing: 0;
  text-align: right;
  margin: 0px 10px;
`

export const WeatherIconDetail = styled.Image`
    width: 140px;
    height: 140px;
`