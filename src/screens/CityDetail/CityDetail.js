import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { ScrollView, Image, View, RefreshControl, Alert, ActivityIndicator, Dimensions } from 'react-native'
import { CurrentDate, WeatherCondition, Temperature, WeatherIconDetail } from './styles'
import { FocusAwareStatusBar } from '../../components/UI'
import { getBackgroundGradient } from '../../utils/background'
import { LinearGradient } from 'expo-linear-gradient'
import { Header } from '../../components/Header'
import { days, monthNames } from '../../utils/calendar'
import { forecastDetailFetch } from '../../actions/WeatherActions'
import { DailyForecastList, HourlyForecastList } from '../../components/List'

const height = Dimensions.get('window').height

const CityDetail = ({ route, navigation, forecastDetailFetch, forecastDetail }) => {
    const { city } = route.params
    const [loading, setLoading] = useState(false)
    const [loaded, setLoaded] = useState(false)
    const errorHandler = () => {
        setLoading(false)
        Alert.alert("Si è verificato un errore, riprova più tardi")
    }

    const fetchData = (refresh) => {
        if (refresh) {
            setLoading(true)
        }
        forecastDetailFetch(city)
            .then(() => {
                setLoading(false)
                setLoaded(true)
            })
            .catch(() => errorHandler())
    }

    useEffect(() => {
        fetchData()
    }, [])

    const getTemperature = (temp) => {
        if (temp < 1 && temp > -1) {
            return 0
        }
        return temp.toFixed(0)
    }
    if (!forecastDetail && !loaded) {
        return (
            <LinearGradient
                colors={getBackgroundGradient(city?.weather[0]?.icon)}
                start={[1, 0]} end={[0, 1]}
                style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="#FFFFFF" />
            </LinearGradient>
        )
    }
    const { daily, hourly, current } = forecastDetail
    const dateCity = new Date(current.dt * 1000)
    return (
        <LinearGradient
            colors={getBackgroundGradient(city?.weather[0]?.icon)}
            start={[1, 0]} end={[0, 1]}
            style={{ flex: 1 }}>
            <Header
                title={city.name}
                pressLeft={() => navigation.goBack()}
                leftButtonHeader={<Image source={require('../../assets/icons/Arrow-Left.png')} style={{ width: 24, height: 24 }} resizeMode="contain" />}
                rightButtonHeader={<Image source={require('../../assets/icons/Plus-White.png')} style={{ width: 24, height: 24 }} resizeMode="contain" />}
            />
            <CurrentDate>{`${days[dateCity.getDay()]} ${dateCity.getDate()}, ${monthNames[dateCity.getMonth()]}`}</CurrentDate>
            <WeatherCondition>{current?.weather[0].main}</WeatherCondition>
            <ScrollView
                style={{ flexGrow: 1 }}
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                        tintColor="white"
                        refreshing={loading}
                        onRefresh={() => fetchData(true)}
                    />
                }>
                <FocusAwareStatusBar barStyle="light-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center' }}>
                    <WeatherIconDetail source={{ uri: `http://openweathermap.org/img/wn/${city?.weather[0]?.icon}@2x.png` }} /*source={require('../assets/icons/ModRainSwrsDay.png')}*/ />
                    <Temperature>{getTemperature(current?.temp)}°</Temperature>
                </View>
                <HourlyForecastList hourly={hourly} />
                <View style={{ height: 45 }} />
                <DailyForecastList daily={daily} />
                <View style={{ height: height * .25 }} />
            </ScrollView>
        </LinearGradient>
    )
}

const mapStateToProps = state => ({
    forecastDetail: state.menu.forecastDetail
})

export default connect(mapStateToProps, { forecastDetailFetch })(CityDetail);