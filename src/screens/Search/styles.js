import styled from 'styled-components/native'

export const StyledSafeAreaView = styled.View`
  flex: 1 100%;
  background-color: #FFFFFF;
  padding-top: ${(props) => props.insets?.top || 0}px;
  padding-bottom: 0px;
`