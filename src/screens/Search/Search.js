import React, { useState } from 'react'
import { View, ScrollView, Dimensions } from 'react-native'
import { FocusAwareStatusBar } from '../../components/UI'
import { StyledSafeAreaView } from './styles'
import { SearchBox } from '../../components/Input'
import LottieView from 'lottie-react-native'
import { useIsFocused } from '@react-navigation/native'
import { BACKGROUND_COLOR } from '../../actions/types'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

const { width } = Dimensions.get('window')

const Search = () => {
    const [searchValue, setSearchValue] = useState('')
    const insets = useSafeAreaInsets()
    const isFocused = useIsFocused()
    return (
        <StyledSafeAreaView insets={insets}>
            <FocusAwareStatusBar backgroundColor={BACKGROUND_COLOR} barStyle='dark-content' />
            <View style={{ flex: 1 }}>
                <SearchBox
                    value={searchValue}
                    onChange={(value) => setSearchValue(value)}
                    placeholder="Turin..."
                />
                <ScrollView contentContainerStyle={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
                    {isFocused ?
                        <LottieView source={require('../../assets/animations/weather-icon.json')} style={{ width: width * .55, alignSelf: 'center' }} autoPlay loop />
                        :
                        null}
                </ScrollView>
            </View>
        </StyledSafeAreaView>
    )
}

export default Search