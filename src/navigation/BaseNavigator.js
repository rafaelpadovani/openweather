import * as React from 'react'
import { View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import { BACKGROUND_COLOR } from '../actions/types'
import { TabBar } from "./TabBar"
import { FocusAwareStatusBar } from '../components/UI'
//SCREENS
import Home from '../screens/Home/Home'
import CityDetail from '../screens/CityDetail/CityDetail'
import Search from '../screens/Search/Search'

const ViewComponent = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
            <FocusAwareStatusBar backgroundColor={BACKGROUND_COLOR} barStyle='dark-content' />
        </View>
    )
}

const HomeStack = createStackNavigator();
const HomeNavigator = props => {
    return (
        <HomeStack.Navigator headerMode="none" headerBackTitleVisible={false}>
            <HomeStack.Screen name="Home" component={Home} />
            <HomeStack.Screen name="CityDetail" component={CityDetail} />
        </HomeStack.Navigator>
    )
}

const LocalizationStack = createStackNavigator();
const LocalizationNavigator = props => {
    return (
        <LocalizationStack.Navigator headerMode="none" headerBackTitleVisible={false}>
            <LocalizationStack.Screen name="Localization" component={ViewComponent} />
        </LocalizationStack.Navigator>
    )
}

const SearchStack = createStackNavigator();
const SearchNavigator = props => {
    return (
        <SearchStack.Navigator headerMode="none" headerBackTitleVisible={false}>
            <SearchStack.Screen name="Search" component={Search} />
        </SearchStack.Navigator>
    )
}

const Tab = createBottomTabNavigator();
const TabNavigator = props => {
    return (
        <Tab.Navigator
            tabBar={(props) => <TabBar {...props} />}>
            <Tab.Screen name="Home" component={HomeNavigator} />
            <Tab.Screen name="Search" component={SearchNavigator} />
            <Tab.Screen name="Localization" component={LocalizationNavigator} />
        </Tab.Navigator >
    )
}

const Stack = createStackNavigator();
const BaseNavigatorContainer = props => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={props.user ? 'App' : 'Auth'} headerMode="none" headerBackTitleVisible={false}>
                <Stack.Screen name="App">
                    {screenProps => <TabNavigator {...screenProps} />}
                </Stack.Screen>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default BaseNavigatorContainer