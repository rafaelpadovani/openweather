import React, { useState, useEffect } from "react"
import {
  View,
  TouchableOpacity,
  Dimensions,
  Animated,
  StyleSheet,
  Platform,
  Image
} from "react-native"
import { ITEM_TAB_COLOR, ITEM_TAB_COLOR_ACTIVE, TAB_NAVIGATOR_COLOR } from "../actions/types"
import styled from 'styled-components/native'

const width = Dimensions.get('window').width

const TabBarContainer = styled.View`
  height: 78px;
  border-radius: 25px;
  background-color: #FFFFFF;
  box-shadow: 5px 10px 20px rgba(0,0,0,0.17);
  position: absolute;
  bottom: 20px;
  padding-top: 25px;
  width: ${width * .9}px;
  alignSelf: center;
  elevation: 7;
`

export const TabBar = ({
  state,
  descriptors,
  navigation,
}) => {
  const [translateValue] = useState(new Animated.Value(0))
  const tabWidth = (width * .89) / 3;

  const animateSlider = (index) => {
    Animated.spring(translateValue, {
      toValue: index * tabWidth,
      velocity: 10,
      useNativeDriver: true,
    }).start();
  };

  const getTabImage = (name, focused) => {
    switch (name) {
      case 'Home':
        return <Image
          source={require('../assets/icons/Home.png')}
          style={{ height: 26, width: 26, tintColor: focused ? ITEM_TAB_COLOR_ACTIVE : ITEM_TAB_COLOR }}
          tintColor={focused ? ITEM_TAB_COLOR_ACTIVE : ITEM_TAB_COLOR}
          resizeMode="contain"
        />
      case 'Search':
        return <Image
          source={require('../assets/icons/Search.png')}
          style={{ height: 26, width: 26, tintColor: focused ? ITEM_TAB_COLOR_ACTIVE : ITEM_TAB_COLOR }}
          tintColor={focused ? ITEM_TAB_COLOR_ACTIVE : ITEM_TAB_COLOR}
          resizeMode="contain"
        />
      case 'Localization':
        return <Image
          source={require('../assets/icons/Location.png')}
          style={{ height: 26, width: 26, tintColor: focused ? ITEM_TAB_COLOR_ACTIVE : ITEM_TAB_COLOR }}
          tintColor={focused ? ITEM_TAB_COLOR_ACTIVE : ITEM_TAB_COLOR}
          resizeMode="contain"
        />
      default:
        return
    }
  }

  useEffect(() => {
    animateSlider(state.index)
  }, [state.index])
  return (
    <TabBarContainer>
      <View style={{ flexDirection: "row" }}>
        <View style={{ marginLeft: width * .065 /*38*/, width: width * .72, position: "absolute", bottom: -27, /*borderWidth: 1, borderColor: 'red'*/ }}>
          <Animated.View
            style={[
              style.slider,
              {
                transform: [{ translateX: translateValue }],
                width: 71,
              },
            ]}
          />
        </View>

        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
                ? options.title
                : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }

            animateSlider(index);
          };

          const onLongPress = () => {
            navigation.emit({
              type: "tabLongPress",
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityStates={isFocused ? ["selected"] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
              key={index}
            >
              {getTabImage(route.name, isFocused)}
            </TouchableOpacity>
          );
        })}
      </View>
    </TabBarContainer>
  );
};

const style = StyleSheet.create({
  slider: {
    height: 2,
    backgroundColor: ITEM_TAB_COLOR_ACTIVE,
    borderRadius: 2,
  },
});
