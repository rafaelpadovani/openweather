import {
    API_URL,
    APPID,
    SET_CITIES,
    SET_FORECAST_DETAIL
} from '../actions/types'

export const multipleCities = (cities) => {
    const parsedCities = cities.join()
    return (dispatch) => {
        // console.log(parsedCities);
        return new Promise((resolve, reject) => {
            // console.log(API_URL + `group?id=${parsedCities}&units=metric&APPID=${APPID}`);
            fetch(API_URL + `group?id=${parsedCities}&units=metric&APPID=${APPID}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then((response) => {
                    // console.log(JSON.stringify((response)))
                    return response.json()
                })
                .then((responseJson) => {
                    // console.log(JSON.stringify((responseJson)))
                    multipleCitiesSuccess(dispatch, responseJson.list)
                    resolve(responseJson)
                })
                .catch((error) => {
                    console.error(error);
                    reject("Si è verificato un errore, riprova più tardi.")
                });
        })
    };
}

const multipleCitiesSuccess = (dispatch, list) => {
    dispatch({
        type: SET_CITIES,
        payload: list
    });
};

export const forecastDetailFetch = (city) => {
    const { lat, lon } = city.coord
    return (dispatch) => {
        // console.log(lat, lon);
        return new Promise((resolve, reject) => {
            // console.log(API_URL + `onecall?lat=${lat}&lon=${lon}&units=metric&APPID=${APPID}`);
            fetch(API_URL + `onecall?lat=${lat}&lon=${lon}&units=metric&APPID=${APPID}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then((response) => {
                    // console.log(JSON.stringify((response)))
                    return response.json()
                })
                .then((responseJson) => {
                    // console.log(JSON.stringify((responseJson)))
                    forecastDetailSuccess(dispatch, responseJson)
                    resolve(responseJson)
                })
                .catch((error) => {
                    console.error(error);
                    reject("Si è verificato un errore, riprova più tardi.")
                });
        })
    };
}

const forecastDetailSuccess = (dispatch, forecastDetail) => {
    dispatch({
        type: SET_FORECAST_DETAIL,
        payload: forecastDetail
    });
};