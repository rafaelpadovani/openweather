import styled from 'styled-components/native'

export const SearchContainer = styled.TouchableOpacity`
    margin-vertical: 15px;
    height: 50px;
    border-radius: 28px;
    background-color: white;
    padding-horizontal: 30px;
    padding-right: 35px;
    justify-content: center;
    shadowColor: #000;
    shadow-offset: 0px 5px;
    shadow-opacity: 0.15;
    shadow-radius: 8.84px;
    elevation: 4;
    margin-horizontal: 20px;
`

export const SearchInput = styled.TextInput`
    flex: 1;
    color: black;
    font-family: Poppins_500Medium;
    font-size: 16px;
    text-align: left;
`