import { Dimensions } from 'react-native'
import styled from 'styled-components/native'

const { height, width } = Dimensions.get('window')

export const HeaderContainer = styled.View`
    background-color: transparent;
    flex-direction: row;
    margin-vertical: 10px;
    justify-content: space-between;
    align-items: center;
    align-content: center;
    padding-top: ${height >= 812 ? '40' : '10'}px;
    z-index: 2;
`

export const Title = styled.Text`
    color: #FFFFFF;
    font-size: 32px;
    text-align: center;
    padding: 5px;
    font-family: Poppins_600SemiBold;
    text-transform: uppercase;
`