import { Dimensions } from 'react-native'
import styled from 'styled-components/native'
const { height, width } = Dimensions.get('window')

export const LoadingContainer = styled.View`
flex: 1;
background-color: rgba(255,255,255,0.7);
justify-content: center;
padding-horizontal: 10px;
align-items: center;
position: absolute;
height: ${height}px;
width: ${width}px;
`