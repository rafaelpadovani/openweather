import styled from 'styled-components/native'

export const HourlyText = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_300Light;
    font-size: 12px;
    font-weight: 300;
    letter-spacing: 0;
    line-height: 27px;
    height: 30px;
    text-align: center;
`

export const NowText = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_700Bold;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 27px;
    height: 30px;
    text-align: center;
`

export const HourlyTemperature = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_300Light;
    font-size: 20px;
    font-weight: 300;
    letter-spacing: 0;
    line-height: 38px;
    height: 30px;
    text-align: right;
`

export const CurrentTemperature = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_700Bold;
    font-size: 25px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 38px;
    height: 30px;
    text-align: right;
`
