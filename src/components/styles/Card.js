import styled from 'styled-components/native'

export const CardContainer = styled.TouchableOpacity`
    height: 140px;
    margin: 10px 20px;
    box-shadow: 5px 10px 15px rgba(0,0,0,0.2);
`

export const CityName = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_600SemiBold;
    font-size: 26px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 29px;
`

export const StyledDate = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_500Medium;
    font-size: 15px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 18px;
`

export const Time = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_300Light;
    font-size: 12px;
    font-weight: 300;
    letter-spacing: 0;
    line-height: 18px;
`

export const WeatherIcon = styled.Image`
    width: 110px;
    height: 110px;
`

export const Temperature = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_700Bold;
    font-size: 50px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 76px;
    text-align: right;
`

export const CardRectangle = styled.View`
    height: 232px;
    width: 148px;
    padding: 10px;
    align-items: center;
    border-radius: 20px;
    background-color: rgba(255,255,255,0.1);
    box-shadow: 5px 10px 20px rgba(0,0,0,0.17); 
`

export const TemperatureDetail = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_700Bold;
    font-size: 30px;
    font-weight: bold;
    letter-spacing: 0;
    margin: 20px 0px 0px;
    text-align: center;
`

export const DailyTitle = styled.Text`
    color: #FFFFFF;
    font-family: Poppins_600SemiBold;
    font-size: 19px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 33px;
    text-align: center;
`

export const DailyWeatherIcon = styled.Image`
    width: 110px;
    height: 110px;
`