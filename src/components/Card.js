import React from 'react'
import { View, Dimensions } from 'react-native'
import { CardContainer, CityName, StyledDate, Time, WeatherIcon, Temperature, CardRectangle, TemperatureDetail, DailyTitle, DailyWeatherIcon } from './styles/Card'
import { LinearGradient } from 'expo-linear-gradient'
import { getBackgroundGradient } from '../utils/background'
import { days, monthNames } from '../utils/calendar'

const width = Dimensions.get('window').width

export const CityCard = ({ city, onPress }) => {
    const getTemperature = (temp) => {
        if (temp < 1 && temp > -1) {
            return 0
        }
        return temp.toFixed(0)
    }
    const getMinutes = (minutes) => {
        if (minutes < 10) {
            return '0' + minutes
        }
        return minutes
    }
    const dateCity = new Date(city.dt * 1000 + (city.sys.timezone * 1000))
    return (
        <CardContainer onPress={onPress}>
            <LinearGradient
                colors={getBackgroundGradient(city?.weather[0]?.icon)}
                start={[0, 1]} end={[1, 0]}
                style={{ flexDirection: 'row', padding: 20, alignItems: 'center', justifyContent: 'space-between', borderRadius: 25, height: '100%' }}>
                <View style={{ width: width * .3 }}>
                    <CityName>{city?.name}</CityName>
                    <StyledDate>{`${days[dateCity.getDay()]} ${dateCity.getDate()},\n${monthNames[dateCity.getMonth()]}`}</StyledDate>
                    <Time>{dateCity.getHours()}.{getMinutes(dateCity.getMinutes())}</Time>
                </View>
                <View style={{ width: width * .2, alignItems: 'center' }}>
                    <WeatherIcon source={{ uri: `http://openweathermap.org/img/wn/${city?.weather[0]?.icon}@2x.png` }} /*source={require('../assets/icons/ModRainSwrsDay.png')}*/ />
                </View>
                <View style={{ width: width * .2, alignItems: 'center' }}>
                    <Temperature>{getTemperature(city?.main?.temp)}°</Temperature>
                </View>
            </LinearGradient>
        </CardContainer>
    )
}

export const DayForecastCard = ({ day }) => {
    const dateCity = new Date(day.dt * 1000)
    const parsedTemp = (temp) => {
        if (temp < 1 && temp > -1) {
            return 0
        }
        return temp.toFixed(0)
    }
    const getTemperature = (temp) => {
        const temperatureRange = Object.values(temp)
        const maxTemp = Math.max.apply(null, temperatureRange)
        const minTemp = Math.min.apply(null, temperatureRange)
        return parsedTemp(minTemp) + '° - ' + parsedTemp(maxTemp)
    }
    return (
        <CardRectangle>
            <DailyTitle>{`${days[dateCity.getDay()]}`}</DailyTitle>
            <TemperatureDetail>{getTemperature(day?.temp)}°</TemperatureDetail>
            <DailyWeatherIcon source={{ uri: `http://openweathermap.org/img/wn/${day?.weather[0]?.icon}@2x.png` }} />
        </CardRectangle>
    )
}
