import React from 'react'
import { FlatList, View, Text } from 'react-native'
import { DayForecastCard } from './Card'
import { NowText, HourlyText, HourlyTemperature, CurrentTemperature } from './styles/List'

export const DailyForecastList = ({ daily }) => {
    return (
        <FlatList
            horizontal
            keyExtractor={(item) => item?.dt.toString()}
            data={daily.slice(1, daily.length)}
            renderItem={({ item }) => (
                <DayForecastCard day={item} />
            )}
            ItemSeparatorComponent={() => <View style={{ width: 20 }} />}
            ListHeaderComponent={<View style={{ width: 20 }} />}
            ListFooterComponent={<View style={{ width: 20 }} />}
            showsHorizontalScrollIndicator={false}
        />
    )
}

const TimeLineItem = ({ hour, index }) => {
    const getTemperature = (temp) => {
        if (temp < 1 && temp > -1) {
            return 0
        }
        return temp.toFixed(0)
    }
    const dateCity = new Date(hour.dt * 1000)
    return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            {index === 0 ?
                <NowText>Now</NowText>
                :
                <HourlyText>{dateCity.getHours() >= 10 ? dateCity.getHours() : '0' + dateCity.getHours()}</HourlyText>
            }
            {index === 0 ?
                <View style={{ height: 25, width: 25, backgroundColor: '#FFFFFF', borderRadius: 13 }} />
                :
                <View style={{ height: 15, width: 15, backgroundColor: '#FFFFFF', borderRadius: 10, alignSelf: 'baseline' }} />
            }
            {index === 0 ?
                <CurrentTemperature>{getTemperature(hour?.temp)}°</CurrentTemperature>
                :
                <HourlyTemperature>{getTemperature(hour?.temp)}°</HourlyTemperature>
            }
        </View>
    )
}

export const HourlyForecastList = ({ hourly, current }) => {
    return (
        <FlatList
            horizontal
            keyExtractor={(item) => item?.dt.toString()}
            data={hourly}
            renderItem={({ item, index }) => (
                <TimeLineItem hour={item} index={index} />
            )}
            ItemSeparatorComponent={() => <View style={{ marginLeft: -12, width: 55, height: 5, backgroundColor: '#FFFFFF', alignSelf: 'center' }} />}
            ListHeaderComponent={<View style={{ width: 45 }} />}
            ListFooterComponent={<View style={{ width: 45 }} />}
            showsHorizontalScrollIndicator={false}
        />
    )
}