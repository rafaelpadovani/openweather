import React from 'react'
import {
    TouchableOpacity,
    View,
    Dimensions,
} from 'react-native'
import { FocusAwareStatusBar } from './UI'
import { HeaderContainer, Title } from './styles/Header'

const { width } = Dimensions.get('window')

export const Header = (props) => {
    return (
        <HeaderContainer>
            <FocusAwareStatusBar barStyle="dark-content" />
            <TouchableOpacity style={{ width: 30, marginLeft: 15 }} onPress={() => props.pressLeft()}>
                {props.leftButtonHeader}
            </TouchableOpacity>
            <Title>
                {(props.title?.length > width * .09) ?
                    props.title?.substr(0, width * .08).toUpperCase() + '...' :
                    props.title?.toUpperCase()}
            </Title>
            <TouchableOpacity style={{ width: 45 }} onPress={props.onPressRight}>
                {props.rightButtonHeader ? props.rightButtonHeader : <View style={{ width: 10 }} />}
            </TouchableOpacity>
        </HeaderContainer>
    )
}