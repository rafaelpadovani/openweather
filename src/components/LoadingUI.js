import React from 'react'
import { Dimensions } from 'react-native'
import LottieView from 'lottie-react-native'
import { LoadingContainer } from './styles/LoadingUI'

const { width } = Dimensions.get('window')

export const LoadingUI = () => {
    return (
        <LoadingContainer>
            <LottieView source={require('../assets/animations/sun-burst-weather-icon.json')} style={{ width: width * .8 }} autoPlay loop />
        </LoadingContainer>
    )
}