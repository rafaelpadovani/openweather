import React from 'react'
import { View, Image } from 'react-native'
import { PRIMARY_COLOR } from '../actions/types'
import { SearchContainer, SearchInput } from './styles/Input'

export const SearchBox = ({ value, onChange, placeholder, keyboardType, secureTextEntry, searchSubmit, autoCompleteType, autoCapitalize }) => {
    return (
        <SearchContainer>
            <SearchInput
                autoCapitalize={autoCapitalize}
                autoCompleteType={autoCompleteType}
                keyboardType={keyboardType}
                value={value}
                placeholder={placeholder}
                placeholderTextColor="#C7C7C7"
                secureTextEntry={secureTextEntry}
                selectionColor={PRIMARY_COLOR}
                onChangeText={(text) => onChange(text)}
                onSubmitEditing={searchSubmit}
            />
            <View style={{ position: 'absolute', top: 0, bottom: 0, right: 20, justifyContent: 'center' }}>
                <Image
                    source={require('../assets/icons/Search.png')}
                    style={{ height: 20, width: 20, tintColor: PRIMARY_COLOR }}
                    tintColor={PRIMARY_COLOR}
                    resizeMode="contain"
                />
            </View>
        </SearchContainer>
    )
}