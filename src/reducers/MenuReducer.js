import {
    SET_CITIES,
    SET_FORECAST_DETAIL
} from '../actions/types';

const INITIAL_STATE = {
    cities: [],
    forecastDetail: null,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_CITIES:
            return { ...state, cities: action.payload };
        case SET_FORECAST_DETAIL:
            return { ...state, forecastDetail: action.payload };
        default:
            return state;
    }
}